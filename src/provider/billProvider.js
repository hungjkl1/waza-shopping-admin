import { GET_LIST,UPDATE,GET_ONE,DELETE } from 'react-admin';
import _ from 'lodash';
import API from '../core/service';
const service = new API();
const billProvider = (type,params) => {
    switch (type) {
        case GET_LIST: {
            return service.post('getBills',params).then((response) => {
                const data = _.get(response.data, 'results',{});
                const total = _.get(response.data, 'count');

                data.forEach(element => {
                    element.id = element.objectId
                });
                console.log(data)
                return Promise.resolve({
                    data,
                    total
                })
              }).catch((err) => {
                throw new Error(err);
              });
        }
        case UPDATE:{
            return service.post('updateBillState',params.data).then((response) => {
                const data = _.get(response, 'data',{});
                data.id = data.objectId
                if(data.message){
                    throw new Error(data.message)
                }
                // const data = _.get(response.data, 'responses',{});
                return Promise.resolve({data:data})
            }).catch((err) => {
                return Promise.resolve({data:{err,id:-1}})
            });
        }
        case GET_ONE:{
            return service.post('getBillAdmin',params).then((response) => {
                const data = _.get(response, 'data',{});
                data.id = data.objectId
                if(data.message){
                    throw new Error(data.message)
                }
                // const data = _.get(response.data, 'responses',{});
                return Promise.resolve({data:data})
            }).catch((err) => {
                return Promise.resolve({data:{err,id:-1}})
            });
        }
        case DELETE:{
            return service.post('deleteAdmin',params).then((response) => {
                const data = _.get(response, 'data',{});
                data.id = data.objectId
                if(data.message){
                    throw new Error(data.message)
                }
                // const data = _.get(response.data, 'responses',{});
                return Promise.resolve({data:data})
            }).catch((err) => {
                return Promise.resolve({data:{err,id:-1}})
            });
        }
        default:
            throw new Error(`Unsupported Data Provider request type ${type}`);
    }
}

export default billProvider