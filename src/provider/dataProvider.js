import AdminProvider from './adminProvider'
import ShopProvider from './shopProvider';
import ProductProvider from './productProvider';
import billProvider from './billProvider';
import passengerProvider from './passengerProvider';
import BillDetailProvider from './billDetailProvider';
export default (type, resource, params) => {
    console.log(params)
    console.log(resource)
    switch (resource) {
        case 'admins':
            return AdminProvider(type,params)
        case 'shops':
            return ShopProvider(type,params)
        case 'products':
            return ProductProvider(type,params)
        case 'bills':
            return billProvider(type,params)
        case 'passengers':
            return passengerProvider(type,params)
        case 'billDetails':
            return BillDetailProvider(type,params)
            default:
            break;
    }
};