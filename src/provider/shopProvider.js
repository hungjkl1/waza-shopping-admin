import { GET_LIST,CREATE,UPDATE,GET_ONE,DELETE,GET_MANY } from 'react-admin';
import _ from 'lodash';
import API from '../core/service';
const service = new API();
const ShopProvider = (type,params) => {
    switch (type) {
        case GET_LIST: {
            return service.post('getActiveShops',params).then((response) => {
                const data = _.get(response.data, 'results',{});
                const total = _.get(response.data, 'count');

                data.forEach(element => {
                    element.id = element.objectId
                });
                return Promise.resolve({
                    data,
                    total
                })
              }).catch((err) => {
                throw new Error(err);
              });
        }
        case CREATE: {
            return service.post('createShop',params.data).then((response) => {
                const data = _.get(response, 'data',{});
                data.id = data.objectId
                if(data.message){
                    throw new Error(data.message)
                }
                // const data = _.get(response.data, 'responses',{});
                return Promise.resolve({data:data})
            }).catch((err) => {
                return Promise.resolve({data:{err,id:-1}})
            });
        }
        case UPDATE:{
            return service.post('updateShop',params.data).then((response) => {
                const data = _.get(response, 'data',{});
                data.id = data.objectId
                if(data.message){
                    throw new Error(data.message)
                }
                // const data = _.get(response.data, 'responses',{});
                return Promise.resolve({data:data})
            }).catch((err) => {
                return Promise.resolve({data:{err,id:-1}})
            });
        }
        case GET_ONE:{
            return service.post('getShop',params).then((response) => {
                const data = _.get(response, 'data',{});
                data.id = data.objectId
                if(data.message){
                    throw new Error(data.message)
                }
                // const data = _.get(response.data, 'responses',{});
                return Promise.resolve({data:data})
            }).catch((err) => {
                return Promise.resolve({data:{err,id:-1}})
            });
        }
        case DELETE:{
            return service.post('removeShop',params).then((response) => {
                const data = _.get(response, 'data',{});
                data.id = data.objectId
                if(data.message){
                    throw new Error(data.message)
                }
                // const data = _.get(response.data, 'responses',{});
                return Promise.resolve({data:data})
            }).catch((err) => {
                return Promise.resolve({data:{err,id:-1}})
            });
        }
        case GET_MANY:{
            return service.post('getShopFromArray',params).then((response) => {
                const data = _.get(response, 'data',{});
                data.forEach(element => {
                    element.id = element.objectId
                });
                if(data.message){
                    throw new Error(data.message)
                }
                // const data = _.get(response.data, 'responses',{});
                return Promise.resolve({data:data})
            }).catch((err) => {
                return Promise.resolve({data:{err,id:-1}})
            });
        }
        default:
            throw new Error(`Unsupported Data Provider request type ${type}`);
    }
}

export default ShopProvider