import { GET_MANY_REFERENCE } from 'react-admin';
import _ from 'lodash';
import API from '../core/service';
const service = new API();
const BillDetailProvider = (type,params) => {
    switch (type) {
        case GET_MANY_REFERENCE: {
            return service.post('getBill',params).then((response) => {
                console.log(response.data)
                const data = _.get(response.data, 'results',{});
                const total = _.get(response.data, 'count');

                data.forEach(element => {
                    element.id = element.objectId
                });
                return Promise.resolve({
                    data,
                    total
                })
              }).catch((err) => {
                throw new Error(err);
              });
        }
        default:
            throw new Error(`Unsupported Data Provider request type ${type}`);
    }
}

export default BillDetailProvider