import React from 'react'
import { Admin, Resource } from 'react-admin'
import  UserList  from './layout/admins'
import CreateAdmin from './layout/admins/createAdmin'
import EditAdmin from './layout/admins/editAdmin'
import dataProvider from './provider/dataProvider'
import ViewAdmin from './layout/admins/viewAdmin'
import authProvider from './provider/authProvider'
import ShopList from './layout/shops'
import ViewShop from './layout/shops/viewShop'
import CreateShop from './layout/shops/createShop'
import EditShop from './layout/shops/editShop'
import EditProduct from './layout/products/editProduct'
import CreateProduct from './layout/products/createProduct'
import ViewProduct from './layout/products/viewProduct'
import BillList from './layout/bills'
import PassengerList from './layout/passenger'
import viewPassenger from './layout/passenger/view'
import ViewBill from './layout/bills/view'
import EditBill from './layout/bills/edit'

const App = () => (
  <Admin dataProvider={dataProvider} authProvider={authProvider}>
    <Resource name="admins" list={UserList} show={ViewAdmin} create={CreateAdmin} edit={EditAdmin} />
    <Resource name="shops" list={ShopList} show={ViewShop} create={CreateShop} edit={EditShop} />
    <Resource name="products" show={ViewProduct} create={CreateProduct} edit={EditProduct} />
    <Resource name="bills" list={BillList} show={ViewBill} edit={EditBill}/>
    <Resource name="billDetails" />
    <Resource name="passengers" list={PassengerList} show={viewPassenger} />
  </Admin>
  );
export default App;