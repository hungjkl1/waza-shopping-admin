import React from "react";
import { Field } from "redux-form";
import {
  Edit,
  SimpleForm,
  TextInput,
  SelectInput,
  FormDataConsumer,
  DisabledInput
} from "react-admin";
import TextField from "@material-ui/core/TextField";
import city from "./local.json";
import _ from "lodash";
const speacialCharacter = /[!@#$%^&*(),.?":{}|<>\\/]/g;
export const validateShopCreation = values => {
  console.log(values);
  const errors = {};
  // validate shop name
  if (!values.name) {
    errors.name = ["The shop name is required"];
  } else if (values.name.length > 50) {
    errors.name = ["The shop name must be less than 50 characters"];
  } else if (speacialCharacter.test(values.name)) {
    errors.name = ["The shop name cant contain any special character "];
  }
  // validate address
  if (!values.address) {
    errors.address = ["The shop address is required"];
  } else if (values.address.length > 50) {
    errors.address = [
      "The shop address must be more than 1 and less than 50 characters"
    ];
  }
  // validate phone
  if (!values.phone) {
    errors.phone = ["The shop phone number is required"];
  } else if (!Number(values.phone)) {
    errors.phone = ["The shop phone number must be number"];
  } else if (!values.phone.length === 8 || !values.phone.length === 10) {
    errors.phone = ["The shop phone number must be 8 or 10 character"];
  }
  // validate Open-Close time
  if (!values.openTime) {
    errors.openTime = ["The shop open time is required"];
  }
  if (!values.closeTime) {
    errors.closeTime = ["The shop close time is required"];
  }
  // validate Social information
  if (values.socialInfo) {
    if (values.socialInfo.length > 50)
      errors.socialInfo = [
        "The shop social information must be less than 50 characters"
      ];
  }
  return errors;
};
//text field
const renderTextField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}) => (
  <TextField
    type="time"
    label={label}
    error={!!(touched && error)}
    helperText={touched && error}
    inputProps={{
      step: 300 // 5 min
    }}
    InputLabelProps={{
        shrink: true
      }}
    {...input}
    {...custom}
  />
);



const CloseTime = () => (
  <span>
      <Field
        name="openTime"
        component={renderTextField}
        label="Open Time"
        placeholder="latitude"
        style={{marginRight:"40px",width:"200px"}}
      />
      <Field
        name="closeTime"
        component={renderTextField}
        label="Close Time"
        style={{width:"200px"}}
        placeholder="latitude"
      />
  </span>
);
//main create
class EditShop extends React.Component {
  render() {
    console.log(this.input);
    return (
      <Edit {...this.props}>
        <SimpleForm validate={validateShopCreation}>
          <TextInput source="name" />
          <TextInput source="phone" />
          <DisabledInput
            source="city"
          />
          <FormDataConsumer>
            {({ formData, ...rest }) => {
              const currentCity = city.filter(
                city => city.name === formData.city
              );
              return (
                <SelectInput
                  source="district"
                  choices={
                    _.isEmpty(currentCity) ? [] : currentCity[0].districts
                  }
                  optionText="name"
                  optionValue="name"
                />
              );
            }}
          </FormDataConsumer>
          <FormDataConsumer>
            {({ formData, ...rest }) => {
              const currentCity = city.filter(
                city => city.name === formData.city
              );
              const currentDistrict = _.isEmpty(currentCity)
                ? []
                : currentCity[0].districts.filter(
                    district => district.name === formData.district
                  );
              return (
                <SelectInput
                  source="ward"
                  choices={
                    _.isEmpty(currentDistrict) ? [] : currentDistrict[0].wards
                  }
                  optionText="name"
                  optionValue="name"
                />
              );
            }}
          </FormDataConsumer>
          <TextInput source="socialInfo" />
          <br></br>
          <CloseTime />
        </SimpleForm>
      </Edit>
    );
  }
}

export default EditShop;
