import ChatBubbleIcon from "@material-ui/icons/ChatBubble";
import { withStyles } from "@material-ui/core/styles";
import { Button } from "react-admin";
import React from 'react';
import { Link } from 'react-router-dom';
const styles = {
  button: {
    marginTop: "1em",
  },
};

export const AddNewProductButton = withStyles(styles)(({ classes, record }) => (
  <Button
    className={classes.button}
    variant="raised"
    component={Link}
    to={{
      pathname: "/products/create",
      search: `?shopId=${record.id}`,
    }}
    label="Add a Product"
  >
    <ChatBubbleIcon />
  </Button>
));