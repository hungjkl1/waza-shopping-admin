import React from 'react';
import { List, Datagrid, TextField,EditButton,TextInput, Filter,ShowButton } from 'react-admin';

const ShopFilter = (props) => {
    return <Filter {...props}>
        <TextInput label="name" source="name"/>
    </Filter>
;}

const ShopList = props => {
    return <List {...props} filters={<ShopFilter/>}>
        <Datagrid rowClick="show">
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="phone" />
            <TextField source="city" />
            <TextField source="openTime" />
            <TextField source="closeTime" />
            <TextField source="createdAt" />
            <TextField source="updatedAt" />
            <EditButton/>
            <ShowButton/>
        </Datagrid>
    </List>
};
export default ShopList