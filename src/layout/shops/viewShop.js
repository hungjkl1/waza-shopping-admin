import React from "react";
import {
  Show,
  TextField,
  DateField,
  DeleteButton,
  TabbedShowLayout,
  Tab,
  ReferenceManyField,
  Datagrid,
  EditButton,
  ShowButton,
} from "react-admin";
import { AddNewProductButton } from "./button";

const viewAdmin = props => (
  <Show title="Admin Detail" {...props}>
    <TabbedShowLayout>
      <Tab label="Summary">
        <TextField source="id" />
        <TextField source="name" />
        <TextField source="phone" />
        <DateField source="createdAt" />
        <DateField source="updatedAt" />
        <DeleteButton />
      </Tab>
      <Tab label="Detail">
        <TextField source="city" />
        <TextField source="district" />
        <TextField source="ward" />
        <TextField source="address" />
        <TextField source="openTime" />
        <TextField source="closeTime" />
      </Tab>
      <Tab label="product">
        <ReferenceManyField label='' target='' reference="products">
          <Datagrid>
          <TextField source="id" />
          <TextField source="name" />
          <TextField source="description" />
          <TextField source="price" />
          <TextField source="Unit" />
          <TextField source="type" />
          <ShowButton/>
          <EditButton/>
          <DeleteButton/>
          </Datagrid>
        </ReferenceManyField>
        <AddNewProductButton/>
      </Tab>
    </TabbedShowLayout>
  </Show>
);
export default viewAdmin;
