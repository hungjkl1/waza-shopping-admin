import React from "react";
import {
  Show,
  TextField,
  DateField,
  ReferenceField,
  DeleteButton,
  TabbedShowLayout,
  Tab,
  ReferenceManyField,
  Datagrid,
  EditButton,
  ShowButton,
} from "react-admin";
const DetailPrice = ({ record = {} }) => <span>{record.quantity*record.price}</span>
DetailPrice.defaultProps = {
  label: 'Total'
}
const ViewBill = props => (
  <Show title="Admin Detail" {...props}>
    <TabbedShowLayout>
      <Tab label="Summary">
      <TextField source="id" />
      <ReferenceField label="Passenger" source="user.objectId" reference="passengers" linkType="show">
          <TextField source="fullName" />
      </ReferenceField>
      <TextField source="State" />
      <TextField source="start_address" />
      <TextField source="end_address" />
      <DateField source="createdAt" showTime/>
      <DateField source="updatedAt" showTime/>
      <ReferenceField label="Shop" source="shop.objectId" reference="shops" linkType="show">
          <TextField source="name" />
      </ReferenceField>
      </Tab>
      <Tab label="Bill Detail">
        <ReferenceManyField label='Products' target='' reference="billDetails">
          <Datagrid>
          <TextField source="id" />
          <TextField source="product.name" />
          <DetailPrice/>
          <TextField source="quantity" />
          <TextField source="product.Unit" />
          <TextField source="product.price" />
          <TextField source="product.type" />
          <ShowButton/>
          <EditButton/>
          <DeleteButton/>
          </Datagrid>
        </ReferenceManyField>
      </Tab>
    </TabbedShowLayout>
  </Show>
);
export default ViewBill;
