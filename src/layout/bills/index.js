import React from 'react';
import { List, Datagrid, TextField,EditButton,TextInput, Filter,ShowButton,ReferenceField, DateField} from 'react-admin';

const BillFilter = (props) => {
    return <Filter {...props}>
        <TextInput label="name" source="name"/>
    </Filter>
;}

const BillList = props => {
    return <List {...props} filters={<BillFilter/>}>
        <Datagrid rowClick="show">
            <TextField source="id" />
            <ReferenceField label="Passenger" source="user.objectId" reference="passengers" linkType="show">
                <TextField source="fullName" />
            </ReferenceField>
            <TextField source="State" />
            <TextField source="start_address" />
            <TextField source="end_address" />
            <ReferenceField label="Shop" source="shop.objectId" reference="shops" linkType="show">
                <TextField source="name" />
            </ReferenceField>
            <DateField source="createdAt" showTime />
            <DateField source="updatedAt" showTime />
            <EditButton/>
            <ShowButton/>
        </Datagrid>
    </List>
};
export default BillList