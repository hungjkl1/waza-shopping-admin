import React from 'react';
import { Edit,SimpleForm,SelectInput,TextInput } from 'react-admin';
const BillStatus = [{id:'ORDER WAITING',name:'ORDER WAITING'},{name:'ORDER LOOKING FOR DRIVER',id:'ORDER LOOKING FOR DRIVER'},{name:'ORDER HAVE DRIVER',id:'ORDER HAVE DRIVER'},{name:'ORDER IN PROCESS',id:'ORDER IN PROCESS'},{name:'ORDER DELIVERED',id:'ORDER DELIVERED'},{name:'ORDER CANCELED',id:'ORDER CANCELED'},{name:'ORDER CONFIRM FINISH',id:'ORDER CONFIRM FINISH'}]
const EditBill = props => {
    return (
        <Edit {...props}>
            <SimpleForm>
                <TextInput source='id' disabled />

                <SelectInput source='State' choices={BillStatus} />
            </SimpleForm>
        </Edit>
    );
}

export default EditBill;
