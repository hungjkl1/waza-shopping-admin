import React from "react"
import productType from './type.json'
import {
  Create,
  SimpleForm,
  TextInput,
  NumberInput,
  SelectInput,
  ReferenceInput,
} from "react-admin";
import { parse } from 'query-string';
import {validateProduct} from './validate'
//text field
//main create
class CreateProduct extends React.Component {
  isShopId = shop =>{
    if(!shop) return <ReferenceInput label="Shop" source="shop.objectId" reference="shops">
    <SelectInput optionText="name" />
</ReferenceInput>   
  }


  render() {
    const { shopId: objectId } = parse(this.props.location.search);
    const shop = objectId ? {objectId} : '';
    return (
      <Create {...this.props}>
        <SimpleForm defaultValue={{shop}} validate={validateProduct} redirect={`/shops/${shop.objectId}/show/2`}>
          <TextInput source="name" />
          <TextInput source="description" />
          <NumberInput source="price"/>
          <TextInput source="Unit"/>
          <SelectInput
          source="type"
          choices={productType.type}
          optionText="name"
          translateChoice={false}
          optionValue="name"
        />
          {this.isShopId(shop)}
          </SimpleForm>
      </Create>
    );
  }
}

export default CreateProduct;
