import React from 'react';
import { List, Datagrid, TextField,EditButton,TextInput, Filter,ShowButton,ReferenceField} from 'react-admin';

const ProductFilter = (props) => {
    return <Filter {...props}>
        <TextInput label="name" source="name"/>
    </Filter>
;}

const ProductList = props => {
    return <List {...props} filters={<ProductFilter/>}>
        <Datagrid rowClick="show">
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="description" />
            <TextField source="price" />
            <TextField source="type" />
            <ReferenceField label="Shop" source="shop.objectId" reference="shops">
                <TextField source="name" />
            </ReferenceField>
            <EditButton/>
            <ShowButton/>
        </Datagrid>
    </List>
};
export default ProductList