import React from "react";
import productType from './type.json'
import {
  Edit,
  SimpleForm,
  TextInput,
  NumberInput,
  SelectInput
} from "react-admin";
import {validateProduct} from './validate'
const redirect = (basePath, id, data) => `/shops/${data.shop.objectId}/show/2`

class EditProduct extends React.Component {
  render() {
    return (
      <Edit {...this.props}>
        <SimpleForm validate={validateProduct} redirect={redirect}>
          <TextInput source="name" />
          <TextInput source="description" />
          <NumberInput source="price" step={5000}/>
          <TextInput source="Unit"/>
          <SelectInput
          source="type"
          choices={productType.type}
          optionText="name"
          translateChoice={false}
          optionValue="name"
        />
        </SimpleForm>
      </Edit>
    );
  }
}

export default EditProduct;
