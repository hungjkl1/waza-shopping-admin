import React from 'react';
import { Show, SimpleShowLayout, TextField, EditButton, DeleteButton,ReferenceField  } from 'react-admin';

const ViewProduct = (props) => (
    <Show title="Admin Detail" {...props}>
        <SimpleShowLayout>
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="description" />
            <TextField source="price" />
            <TextField source="type" />
            <ReferenceField label="Shop" source="shop.objectId" reference="shops">
                <TextField source="name" />
            </ReferenceField>
            <EditButton/>
            <DeleteButton redirect/>
        </SimpleShowLayout>
    </Show>
);
export default ViewProduct