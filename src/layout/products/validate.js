export const validateProduct = values => {
    const specialCharacter = /[!@#$%^&*(),.?":{}|<>\\/]/g;
    const errors = {};
    if(!values.name){
      errors.name = ['Please provide name']
    } else if(specialCharacter.test(values.name)){
      errors.name = ['Name should not contain any special character']
    }
    if(!values.price){
      errors.price = ['Please provide price']
    }
    if(!values.shop.objectId){
      errors.shop = ['Please provide shop have the product']
    }
    // validate shop name
    return errors;
  };