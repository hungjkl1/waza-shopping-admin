import React from 'react';
import { Create, SimpleForm, TextInput } from 'react-admin';
const validateAdminCreation = (values) => {
    const emailValid = /^[a-z][a-z0-9_.]{5,32}@[a-z0-9]{2,}(.[a-z0-9]{2,4}){1,2}$/
    const errors = {};
    if (!values.username) {
        errors.username = ['The username is required']
    } else if (values.username.length < 8) {
        errors.username = ['username not long enough']
    }
    if (!values.email) {
        errors.email = ['The email is required']
    } else if (!emailValid.test(values.email)) {
        errors.email = ['Wrong email format'];
    }
    if (!values.password) {
        errors.password = ['The password is required']
    } else if (values.password.length < 8) {
        errors.password = ['password is not long enough']
    }
    if (!values.idCard) {
        errors.idCard = ['ID Card is required']
    } else if (values.idCard.length !== 10){
        errors.idCard = ['ID card must be 10 word']
    }
    if(!values.phone) {
        errors.phone = ['Phone is required']
    } else if(!Number(values.phone)) {
        errors.phone = ['Phone must be number']
    }
    return errors
};

const CreateAdmin = props => {
    return (
        <Create {...props}>
            <SimpleForm validate={validateAdminCreation}>
            <TextInput source='username' type='text' />
            <TextInput source='email' type='email' />
            <TextInput source='password' type='password' />
            <TextInput source='phone' />
            <TextInput source='idCard' />
            </SimpleForm>
        </Create>
    );
}

export default CreateAdmin;
