import React from 'react';
import { Show, SimpleShowLayout, TextField, DateField, DeleteButton  } from 'react-admin';

const viewAdmin = (props) => (
    <Show title="Admin Detail" {...props}>
        <SimpleShowLayout>
            <TextField source="id" />
            <TextField source="username" />
            <TextField source="phone" />
            <TextField source="idCard" />
            <DateField source="createdAt" />
            <DateField source="updatedAt" />
            <DeleteButton/>
        </SimpleShowLayout>
    </Show>
);
export default viewAdmin