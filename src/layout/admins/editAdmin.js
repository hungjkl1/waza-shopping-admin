import React from 'react';
import { Edit, SimpleForm, TextInput } from 'react-admin';
export const validateAdminCreation = (values) => {
    const errors = {};
    if (!values.idCard) {
        errors.idCard = ['ID Card is required']
    } else if (values.idCard.length !== 10){
        errors.idCard = ['ID card must be 10 word']
    }
    if(!values.phone) {
        errors.phone = ['Phone is required']
    } if(!values.phone) {
        errors.phone = ['Phone is required']
    } else if(!Number(values.phone)) {
        errors.phone = ['Phone must be number']
    } if(values.password2!==values.password) {
        errors.password = ['Please match two password']
        errors.password2 = ['Please match two password']
    }
    return errors
};

const EditAdmin = props => {
    return (
        <Edit {...props}>
            <SimpleForm validate={validateAdminCreation}>
                <TextInput source='phone' />
                <TextInput source='idCard' />
            </SimpleForm>
        </Edit>
    );
}

export default EditAdmin;
