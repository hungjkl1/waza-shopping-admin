import React from 'react';
import { List, Datagrid, TextField,EditButton,TextInput, Filter,ShowButton } from 'react-admin';

const PostFilter = (props) => {
    return <Filter {...props}>
        <TextInput label="username" source="username"/>
    </Filter>
;}

const UserList = props => {
    return <List {...props} filters={<PostFilter/>}>
        <Datagrid rowClick="show">
            <TextField source="id" />
            <TextField source="username" />
            <TextField source="phone" />
            <TextField source="idCard" />
            <TextField source="createdAt" />
            <TextField source="updatedAt" />
            <EditButton/>
            <ShowButton/>
        </Datagrid>
    </List>
};
export default UserList