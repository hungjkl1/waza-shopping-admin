import React from 'react';
import { Show, SimpleShowLayout, TextField, DateField, DeleteButton  } from 'react-admin';

const viewPassenger = (props) => (
    <Show title="Passenger Detail" {...props}>
        <SimpleShowLayout>
            <TextField source="id" />
            <TextField source="fullName" />
            <TextField source="email" />
            <TextField source="rank" />
            <TextField source="signinCount" />
            <TextField source="wallet" />
            <TextField source="coreId" />
            <DateField source="createdAt" />
            <DateField source="updatedAt" />
            <DeleteButton/>
        </SimpleShowLayout>
    </Show>
);
export default viewPassenger