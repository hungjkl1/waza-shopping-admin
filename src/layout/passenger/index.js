import React from 'react';
import { List, Datagrid, TextField,EditButton,TextInput, Filter,ShowButton } from 'react-admin';

const PassengerFilter = (props) => {
    return <Filter {...props}>
        <TextInput label="fullName" source="username"/>
    </Filter>
;}

const PassengerList = props => {
    return <List {...props} filters={<PassengerFilter/>}>
        <Datagrid rowClick="show">
            <TextField source="id" />
            <TextField source="fullName" />
            <TextField source="phone" />
            <TextField source="createdAt" />
            <TextField source="updatedAt" />
            <EditButton/>
            <ShowButton/>
        </Datagrid>
    </List>
};
export default PassengerList