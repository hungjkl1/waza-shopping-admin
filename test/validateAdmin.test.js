const should = require('chai').should();
const validateAdminCreation = require('../src/layout/admins/validateAdmin');

describe('#Test Admin validateAdminCreation()', () => {

  context('Create empty admin', () => {
    it('It should return an object of errors', () => {
      const result = validateAdminCreation({});
      result.should.be.an('object');
      result.should.have.property('username');
      result.should.have.property('email');
      result.should.have.property('password');
      result.should.have.property('idCard');
      result.should.have.property('phone');
    });
  });

  context('Create admin successful', () => {
    it('It should return an empty object', () => {
      const result = validateAdminCreation({
        username: "HelloWorld",
        password: "HelloWorld",
        email: "helloworld@gmail.com",
        idCard: "TH1604PEDE",
        phone: 174837642
      });
      result.should.be.an('object');
      result.should.not.have.any.property;
    });
  });

  // Test user name
  context('With empty username or username is "Hello" ', () => {
    it('It should return "The username is required"', () => {
      const result = validateAdminCreation({}).username;
      result.should.be.an('array').that.include('The username is required');
    });
    it('It should return "username not long enough"', () => {
      const result = validateAdminCreation({
        username: "Hello"
      }).username;
      result.should.be.an('array').that.include('username not long enough');
    });
  });

  // Test password
  context('With empty password or password is "Hello" ', () => {
    it('It should return "The password is required"', () => {
      const result = validateAdminCreation({}).password;
      result.should.be.an('array').that.include('The password is required');
    });
    it('It should return "password is not long enough" ', () => {
      const result = validateAdminCreation({
        password: "Hello"
      }).password;
      result.should.be.an('array').that.include('password is not long enough');
    });
  });

  // Test email
  context('With empty email or email is "Hello@test" ', () => {
    it('It should return "The email is required"', () => {
      const result = validateAdminCreation({}).email;
      result.should.be.an('array').that.include('The email is required');
    });
    it('It should return "Wrong email format"', () => {
      const result = validateAdminCreation({
        email: "Hello@test"
      }).email;
      result.should.be.an('array').that.include('Wrong email format');
    });
  });

  // Test ID Card
  context('With empty ID Card or ID Card is "XYZ123" ', () => {
    it('It should return "ID Card is required', () => {
      const result = validateAdminCreation({}).idCard;
      result.should.be.an('array').that.include('ID Card is required');
    });
    it('It should return "ID card must be 10 word"', () => {
      const result = validateAdminCreation({
        idCard: "XYZ123"
      }).idCard;
      result.should.be.an('array').that.include('ID card must be 10 word');
    });
  });

  // Test phone number
  context('With empty phone numeber or phone number is a string ', () => {
    it('It should return "Phone is required"', () => {
      const result = validateAdminCreation({}).phone;
      result.should.be.an('array').that.include('Phone is required');
    });
    it('It should return "Phone must be number"', () => {
      const result = validateAdminCreation({
        phone: "string"
      }).phone;
      result.should.be.an('array').that.include('Phone must be number');
    });
  });
});